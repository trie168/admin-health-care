/*!

=========================================================
* Argon Dashboard React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import Auth from "services/Auth";

// reactstrap components
import {
    Button,
    Card,
    CardBody,
    FormGroup,
    Form,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Col
} from "reactstrap";

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: "",
            formError: "",
            isSubmitting: false
        };
    }

    handleSubmit = event => {
        event.preventDefault();
        this.setState({ isSubmitting: true, formError: "" });
        const { email, password } = this.state;

        Auth.login({
            email,
            password
        })
            .then(result => {
                this.props.history.push("/admin/index");
            })
            .catch(error => {
                let formError = "Error";
                if (error.status === 401 || error.status === 422) {
                    formError = "Username atau password tidak benar";
                } else if (!error.status) {
                    formError = "Silahkan periksa koneksi Internet";
                }

                this.setState({ isSubmitting: false, formError });
            });
    };
    render() {
        const { email, password, formError, isSubmitting } = this.state;
        return (
            <>
                <Col lg="5" md="7">
                    <Card className="bg-secondary shadow border-0">
                        <CardBody className="px-lg-5 py-lg-5">
                            <Form role="form" onSubmit={this.handleSubmit}>
                                <p className="text-danger form-error">
                                    {formError}
                                </p>
                                <FormGroup className="mb-3">
                                    <InputGroup className="input-group-alternative">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="ni ni-email-83" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input
                                            placeholder="Email"
                                            type="email"
                                            value={email}
                                            onChange={event =>
                                                this.setState({
                                                    email: event.target.value
                                                })
                                            }
                                            required
                                        />
                                    </InputGroup>
                                </FormGroup>
                                <FormGroup>
                                    <InputGroup className="input-group-alternative">
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText>
                                                <i className="ni ni-lock-circle-open" />
                                            </InputGroupText>
                                        </InputGroupAddon>
                                        <Input
                                            placeholder="Password"
                                            type="password"
                                            value={password}
                                            onChange={event =>
                                                this.setState({
                                                    password: event.target.value
                                                })
                                            }
                                            required
                                        />
                                    </InputGroup>
                                </FormGroup>

                                <div className="text-center">
                                    <Button
                                        className="my-4"
                                        color="primary"
                                        type="submit"
                                        disabled={isSubmitting}
                                    >
                                        Sign in
                                    </Button>
                                </div>
                            </Form>
                        </CardBody>
                    </Card>
                </Col>
            </>
        );
    }
}

export default Login;
